﻿const effect = "move"; // copy, move, all

function dragstart_handler(ev) {
  setDropEffects(ev, effect);
  addDifferentTypesOfDragData(ev);
  //dragImage(ev);
}

window.addEventListener('DOMContentLoaded', () => {
  // Get the element by id
  const element = document.getElementById("p1");
  // Add the ondragstart event listener
  element.addEventListener("dragstart", dragstart_handler);
});


function addDifferentTypesOfDragData(ev) {
  // Add the target element's id to the data transfer object
  ev.dataTransfer.setData("text/plain", ev.target.id);
  ev.dataTransfer.setData("text/html", ev.target.outerHTML);
  ev.dataTransfer.setData("text/uri-list", ev.target.ownerDocument.location.href);
}

function setDropEffects(ev, effect) {
  ev.dataTransfer.dropEffect = effect; 
}

function dragover_handler(ev) {
  ev.preventDefault();
  setDropEffects(ev, effect);
}
function drop_handler(ev) {
  ev.preventDefault();
  // Get the id of the target and add the moved element to the target's DOM
  const data = ev.dataTransfer.getData("text/plain");
  ev.target.appendChild(document.getElementById(data));
}


function dragImage(ev) {
  // Create an image and then use it for the drag image.
  // NOTE: change "example.gif" to a real image URL or the image 
  // will not be created and the default drag image will be used.
  let img = new Image();
  img.src = 'img/love.gif';
  ev.dataTransfer.setDragImage(img, 10, 10);
}