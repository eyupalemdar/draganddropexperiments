﻿namespace BlazorDragAndDrop
{
    public enum JobStatuses
    {
        Todo,
        Started,
        Completed
    }
}
